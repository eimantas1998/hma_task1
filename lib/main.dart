import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool text1 = false;
  bool text2 = false;
  bool text3 = false;
  bool text4 = false;

  void _changeColor1() {
    if (!text1) {
      setState(() {
        text1 = true;
      });
    } else {
      setState(() {
        text1 = false;
      });
    }
  }

  void _changeColor2() {
    if (!text2) {
      setState(() {
        text2 = true;
      });
    } else {
      setState(() {
        text2 = false;
      });
    }
  }

  void _changeColor3() {
    if (!text3) {
      setState(() {
        text3 = true;
      });
    } else {
      setState(() {
        text3 = false;
      });
    }
  }

  void _changeColor4() {
    if (!text4) {
      setState(() {
        text4 = true;
      });
    } else {
      setState(() {
        text4 = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.greenAccent,
              height: MediaQuery.of(context).size.height / 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      MaterialButton(
                        height: 60.0,
                        minWidth: 120.0,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Button1',
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                        color: Colors.orange,
                        onPressed: () => _changeColor1(),
                      ),
                      MaterialButton(
                        height: 60.0,
                        minWidth: 120.0,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Button2',
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                        color: Colors.orange,
                        onPressed: () => _changeColor2(),
                      ),
                    ],
                  ),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      MaterialButton(
                        height: 60.0,
                        minWidth: 120.0,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Button3',
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                        color: Colors.orange,
                        onPressed: () => _changeColor3(),
                      ),
                      MaterialButton(
                        height: 60.0,
                        minWidth: 120.0,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Button4',
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                        color: Colors.orange,
                        onPressed: () => _changeColor4(),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 2 - 24,
              width: MediaQuery.of(context).size.width,
              color: Colors.yellow,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    "Text for button 1",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: text1 ? Colors.black : Colors.deepPurple,
                        fontSize: 20),
                  ),
                  Text(
                    "Text for button 2",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: text2 ? Colors.black : Colors.purple,
                        fontSize: 20),
                  ),
                  Text(
                    "Text for button 3",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: text3 ? Colors.black : Colors.green,
                        fontSize: 20),
                  ),
                  Text(
                    "Text for button 4",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: text4 ? Colors.black : Colors.blue,
                        fontSize: 20),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}
